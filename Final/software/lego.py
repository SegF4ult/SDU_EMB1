class Color:
	TRANSPARENT=0
	RED=1
	GREEN=2
	BLUE=4

class Brick:
	pfx = ":/bricks/"
	img = {
		Color.TRANSPARENT:'trans.png',
		Color.RED:'red.png',
		Color.GREEN:'grn.png',
		Color.BLUE:'blu.png',
	}
	@staticmethod
	def getImage(color):
		return Brick.pfx+Brick.img[color]

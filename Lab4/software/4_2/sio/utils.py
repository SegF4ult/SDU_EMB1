#!/usr/bin/env python
#http://eli.thegreenplace.net/2009/07/31/listing-all-serial-ports-on-windows-with-python/

import sys, glob, platform, itertools
from PyQt4.QtCore import QThread, SIGNAL

def getSerialPorts():
	def win():
		import _winreg as winreg
		path='HARDWARE\\DEVICEMAP\\SERIALCOMM'
		try:
			key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, path)
		except WindowsError:
			yield "N/A"
			return
			
		for i in itertools.count():
			try:
				val = winreg.EnumValue(key, i)
				yield str(val[1])
			except EnvironmentError:
				break
	def lin():
		ports = glob.glob("/dev/ttyUSB*")
		if len(ports) <= 0:
			return ["N/A"]
		else:
			return ports
	if platform.system() == "Linux":
		return lin();
	elif platform.system() == "Windows":
		return win();
	
class SerialReader(QThread):
	def __init__(self, ser):
		self.ser = ser
		QThread.__init__(self)
	def start(self, name='Serial',priority = QThread.InheritPriority):
		self.name = name
		QThread.start(self, priority)
	
	def run(self):
		while True:
			try:
				data = self.ser.read(1)
				n = self.ser.inWaiting()
				if n:
					data = data + self.ser.read(n)
					self.emit(SIGNAL("newData(QString,QString)"), (data,self.name))
			except:
				errMsg = "Reader thread is terminated unexpectedly."
				self.emit(SIGNAL("error(QString)"), errMsg)
				break
	
class SerialWriter(QThread):
	def __init__(self, ser):
		self.ser = ser
		QThread.__init__(self)
	def start(self, cmd = "", priority = QThread.InheritPriority):
		self.cmd = cmd
		QThread.start(self, priority)
	
	def run(self):
		try:
			self.ser.write(str(self.cmd))
		except:
			errMsg = "Writer thread is terminated unexpectedly."
			self.emit(SIGNAL("error(QString)"), errMsg)
	
	def terminate(self):
		self.wait()
		QThread.terminate(self)

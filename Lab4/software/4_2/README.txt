===========
== ABOUT ==
===========

This software was made to work on both Windows and Linux.
The software was developed using the following:
	-Python 2.7
	-PyQt4
	-PySerial

=============
== INSTALL ==
=============

(Windows)
For every download, choose the 32-bit version

1. Grab the Python 2.7.* Windows installer from http://www.python.org/download/
2. Download the PyQt4 Windows installer for Py2.7 at http://www.riverbankcomputing.com/software/pyqt/download
3. Follow the Windows link for PySerial at http://pyserial.sourceforge.net/pyserial.html#packages and download the Windows installer

Install the packages in the order you've downloaded them.

(Linux)
Depending on your distribution (I will assume Ubuntu here), you can install it as follows:
	sudo apt-get install python2.7 python-serial python-qt4
	
=============
== EXECUTE ==
=============

To execute the software, run the following:

	python emb1g2_lab3.py

A GUI will pop up, allowing you to select a USB to Serial converter from the dropdown menu.
Upon hitting connect, a measurement thread will open and do its works.


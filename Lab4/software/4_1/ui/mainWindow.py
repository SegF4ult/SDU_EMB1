# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainWindow.ui'
#
# Created: Fri Nov  8 10:33:36 2013
#      by: PyQt4 UI code generator 4.10
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(528, 522)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.cmbPorts = QtGui.QComboBox(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cmbPorts.sizePolicy().hasHeightForWidth())
        self.cmbPorts.setSizePolicy(sizePolicy)
        self.cmbPorts.setMinimumSize(QtCore.QSize(200, 27))
        self.cmbPorts.setObjectName(_fromUtf8("cmbPorts"))
        self.horizontalLayout.addWidget(self.cmbPorts)
        self.btnRefreshPorts = QtGui.QPushButton(self.centralwidget)
        self.btnRefreshPorts.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnRefreshPorts.sizePolicy().hasHeightForWidth())
        self.btnRefreshPorts.setSizePolicy(sizePolicy)
        self.btnRefreshPorts.setMinimumSize(QtCore.QSize(38, 27))
        self.btnRefreshPorts.setMaximumSize(QtCore.QSize(38, 27))
        self.btnRefreshPorts.setText(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/icons/refresh.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btnRefreshPorts.setIcon(icon)
        self.btnRefreshPorts.setIconSize(QtCore.QSize(16, 16))
        self.btnRefreshPorts.setObjectName(_fromUtf8("btnRefreshPorts"))
        self.horizontalLayout.addWidget(self.btnRefreshPorts)
        self.btnConnect = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnConnect.sizePolicy().hasHeightForWidth())
        self.btnConnect.setSizePolicy(sizePolicy)
        self.btnConnect.setMinimumSize(QtCore.QSize(91, 27))
        self.btnConnect.setObjectName(_fromUtf8("btnConnect"))
        self.horizontalLayout.addWidget(self.btnConnect)
        self.btnDisconnect = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnDisconnect.sizePolicy().hasHeightForWidth())
        self.btnDisconnect.setSizePolicy(sizePolicy)
        self.btnDisconnect.setMinimumSize(QtCore.QSize(91, 27))
        self.btnDisconnect.setObjectName(_fromUtf8("btnDisconnect"))
        self.horizontalLayout.addWidget(self.btnDisconnect)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.tabWidget = QtGui.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tabData = QtGui.QWidget()
        self.tabData.setObjectName(_fromUtf8("tabData"))
        self.gridLayout_4 = QtGui.QGridLayout(self.tabData)
        self.gridLayout_4.setObjectName(_fromUtf8("gridLayout_4"))
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.tmpLCD = QtGui.QLCDNumber(self.tabData)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tmpLCD.sizePolicy().hasHeightForWidth())
        self.tmpLCD.setSizePolicy(sizePolicy)
        self.tmpLCD.setFrameShape(QtGui.QFrame.NoFrame)
        self.tmpLCD.setSmallDecimalPoint(False)
        self.tmpLCD.setNumDigits(4)
        self.tmpLCD.setMode(QtGui.QLCDNumber.Dec)
        self.tmpLCD.setSegmentStyle(QtGui.QLCDNumber.Filled)
        self.tmpLCD.setProperty("value", 0.0)
        self.tmpLCD.setObjectName(_fromUtf8("tmpLCD"))
        self.verticalLayout_2.addWidget(self.tmpLCD)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        self.gridLayout_4.addLayout(self.verticalLayout_2, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tabData, _fromUtf8(""))
        self.tabLog = QtGui.QWidget()
        self.tabLog.setObjectName(_fromUtf8("tabLog"))
        self.gridLayout_3 = QtGui.QGridLayout(self.tabLog)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.txtLog = QtGui.QTextEdit(self.tabLog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.txtLog.sizePolicy().hasHeightForWidth())
        self.txtLog.setSizePolicy(sizePolicy)
        self.txtLog.setAutoFillBackground(False)
        self.txtLog.setFrameShape(QtGui.QFrame.StyledPanel)
        self.txtLog.setFrameShadow(QtGui.QFrame.Sunken)
        self.txtLog.setLineWidth(0)
        self.txtLog.setObjectName(_fromUtf8("txtLog"))
        self.gridLayout_3.addWidget(self.txtLog, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tabLog, _fromUtf8(""))
        self.gridLayout.addWidget(self.tabWidget, 1, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionExit = QtGui.QAction(MainWindow)
        self.actionExit.setObjectName(_fromUtf8("actionExit"))

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "EMB1 Group 2 - Lab 4_1", None))
        self.btnConnect.setText(_translate("MainWindow", "Connect", None))
        self.btnDisconnect.setText(_translate("MainWindow", "Disconnect", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabData), _translate("MainWindow", "Data", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabLog), _translate("MainWindow", "Log", None))
        self.actionExit.setText(_translate("MainWindow", "Exit", None))

import mainWindow_rc

#!/usr/bin/env python

import sys, time, serial, glob
from PyQt4.QtGui import QApplication, QMainWindow, QTextCursor, QHeaderView, QLCDNumber, QPixmap, QLabel, QFrame
from PyQt4.QtCore import QObject, SIGNAL, SLOT, QThread, QTimer
from ui.mainWindow import Ui_MainWindow
import ui.mainWindow_rc
from sio.utils import *

regs = { 'adc': (0,1) }

class MeasurementThread(QThread):
	def __init__(self,serial):
		QThread.__init__(self)
		self.ser = serial
		self.stopped = 0
	def start(self, priority=QThread.InheritPriority):
		QThread.start(self, priority)
	def requestData(self,register):
		cmd = 'r'+str(regs[register][0])+str(regs[register][1])
		try:
			self.ser.write(cmd)
		except:
			errMsg = "Writer terminated unexpectedly."
			self.emit(SIGNAL("error(QString)"),errMsg)
		try:
			data = self.ser.read(9)[:8]
		except:
			errMsg = "Reader terminated unexpectedly."
			self.emit(SIGNAL("error(QString)"), errMsg)
		return data
	def run(self):
		while not self.stopped:
			time.sleep(0.1)
			temp = self.requestData('adc')
			self.emit(SIGNAL("newValue(QString)"), temp)
	def stop(self):
		self.stopped = 1
	def terminate(self):
		self.stop()
		self.wait()
		QThread.terminate(self)

class CMainWindow(QMainWindow):
	def __init__(self, *args):
		self.ser = None
		self.mthread = None
		apply(QMainWindow.__init__, (self, ) + args)
		self.ui = Ui_MainWindow()
		self.setupUi()
		self.printLog("Ready...")
	def setupStatusIcon(self):
		self.statusLabel = QLabel("")
		self.ui.statusbar.addPermanentWidget(self.statusLabel)
		self.setStatusIcon()
	def setStatusIcon(self, connected=False):
		if(not connected):
			self.statusLabel.setPixmap(QPixmap(":/icons/disconnected.png"))
		else:
			self.statusLabel.setPixmap(QPixmap(":/icons/connected.png"))
	def setupUi(self):
		self.ui.setupUi(self)
		self.setupStatusIcon()
		self.refreshPorts()

		QObject.connect(self.ui.btnRefreshPorts, SIGNAL("clicked()"), self.refreshPorts)
		QObject.connect(self.ui.btnConnect, SIGNAL("clicked()"), self.connect)
		QObject.connect(self.ui.btnDisconnect, SIGNAL("clicked()"), self.disconnect)
	def closeEvent(self, event):
		self.disconnect()
		event.accept()

	def updateValue(self, data):
		iData = int(str(data),16)
		self.ui.tmpLCD.display(iData)
	def getSelectedPort(self):
		return self.ui.cmbPorts.currentText()

	def refreshPorts(self):
		self.ui.cmbPorts.clear()
		self.ui.cmbPorts.addItems(sorted(getSerialPorts()))

	def connect(self):
		self.disconnect()
		try:
			if(str(self.getSelectedPort())=="N/A"):
				self.errorEvent("No valid port selected");
				raise IOError
			self.printLog("Connecting to %s with 115200 baud." % (self.getSelectedPort()))
			self.ser = serial.Serial(str(self.getSelectedPort()), 115200)
			self.mthread = MeasurementThread(self.ser)
			self.mthread.start()
			self.printLog("Connected successfully.")
			self.setStatusIcon(True)
			QObject.connect(self.mthread, SIGNAL("error(QString)"), self.errorEvent)
			QObject.connect(self.mthread, SIGNAL("newValue(QString)"), self.updateValue)
		except:
			self.ser = None	
			self.setStatusIcon(False)
			self.errorEvent("Failed to connect!")

	def disconnect(self):
		if self.mthread != None:
			if self.mthread.isRunning():
				self.mthread.terminate()
		if self.ser == None: return
		try:
			if self.ser.isOpen:
				self.ser.close()
				self.setStatusIcon(False)
				self.printLog("Disconnected successfully.")
		except:
			self.errorEvent("Failed to disconnect!")
		self.ser = None

	def printLog(self, text):
		self.ui.txtLog.moveCursor(QTextCursor.End)
		self.ui.txtLog.append(text)
		self.ui.txtLog.moveCursor(QTextCursor.End)

	def dataEvent(self, data):
		self.printLog("[DATA] "+data) 

	def errorEvent(self, error):
		self.printLog("[ERROR] "+error)

if __name__ == "__main__":
	app = QApplication(sys.argv)
	mainWindow = CMainWindow()
	mainWindow.show()
	sys.exit(app.exec_())

----------------------------------------------------------------------------------
-- Create Date:		18:35:53 10/29/2013 
-- Module Name:		TOP - Behavioral 
-- Project Name:		SerialADC 
-- Target Devices:	xc3s50an-5tqg144
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity TOP is
	port (
		CLK_50M_I	:	in		std_logic;
		LEDS_O		:	out	std_logic_vector (1 downto 0);
		UART_TX		:	out	std_logic;
		UART_RX		:	in		std_logic;
		
		SPI_CLK		:	BUFFER	std_logic;
		SPI_SDI		:	in		std_logic;
		SPI_SDO		:	out	std_logic;
		SPI_SEL		:	BUFFER	std_logic_vector( 0 downto 0 )
	);
end TOP;

architecture Behavioral of TOP is
	component PseudoTosNet_ctrl is
	port (
		T_clk_50M					:	in		std_logic;
		T_serial_out				:	out	std_logic;
		T_serial_in					:	in		std_logic;
		T_reg_ptr					:	out	std_logic_vector(2 downto 0);
		T_word_ptr					:	out	std_logic_vector(1 downto 0);
		T_data_to_mem				:	in		std_logic_vector(31 downto 0);
		T_data_from_mem			:	out	std_logic_vector(31 downto 0);
		T_data_from_mem_latch	:	out	std_logic
	);
	end component;
	
	component spi_master is
	generic (
		slaves : integer := 1;
		d_width : integer := 2
	);
	port (
		clock   : IN     STD_LOGIC;                             --system clock
		reset_n : IN     STD_LOGIC;                             --asynchronous reset
		enable  : IN     STD_LOGIC;                             --initiate transaction
		cpol    : IN     STD_LOGIC;                             --spi clock polarity
		cpha    : IN     STD_LOGIC;                             --spi clock phase
		cont    : IN     STD_LOGIC;                             --continuous mode command
		clk_div : IN     INTEGER;                               --system clock cycles per 1/2 period of sclk
		addr    : IN     INTEGER;                               --address of slave
		tx_data : IN     STD_LOGIC_VECTOR(d_width-1 DOWNTO 0);  --data to transmit
		miso    : IN     STD_LOGIC;                             --master in, slave out
		sclk    : BUFFER STD_LOGIC;                             --spi clock
		ss_n    : BUFFER STD_LOGIC_VECTOR(slaves-1 DOWNTO 0);   --slave select
		mosi    : OUT    STD_LOGIC;                             --master out, slave in
		busy    : OUT    STD_LOGIC;                             --busy / data ready signal
		rx_data : OUT    STD_LOGIC_VECTOR(d_width-1 DOWNTO 0) --data received
	);
	end component;
	
-- Top-level signals
	signal clk_50M	: std_logic;
	signal clk_cnt	: std_logic_vector(31 downto 0) := (others => '0');
	signal reg0_0 : std_logic_vector(31 downto 0) := (others => '0');
	
-- SPI controller signals
	signal 	spi_rst_n		:	std_logic := '1';
	signal 	spi_en			:	std_logic := '0';
	signal	spi_clk_pol		:	std_logic;
	signal	spi_clk_pha		:	std_logic;
	signal	spi_cont			:	std_logic := '0';
	signal	spi_clk_div		:	integer;
	signal	spi_slv_addr	:	integer;
	signal	spi_tx_data		:	std_logic_vector( 9 downto 0 );
	signal	spi_busy			:	std_logic;
	signal	spi_rx_data		:	std_logic_vector( 9 downto 0 );
	
-- TLC1549 signals
	signal	adc_reading		:	integer;
	
-- uTosNet controller signals
	signal T_reg_ptr					:	std_logic_vector(2 downto 0);
	signal T_word_ptr					:	std_logic_vector(1 downto 0);
	signal T_data_to_mem				:	std_logic_vector(31 downto 0);
	signal T_data_from_mem			:	std_logic_vector(31 downto 0);
	signal T_data_from_mem_latch	:	std_logic;
	
begin
	clk_50M	<=	CLK_50M_I;
	LEDS_O <= clk_cnt(25 downto 24);

	PseudoTosNet_ctrlInst : PseudoTosNet_ctrl
	Port map (
		T_clk_50M					=>	clk_50M,
		T_serial_out				=>	UART_TX,
		T_serial_in					=>	UART_RX,
		T_reg_ptr					=>	T_reg_ptr,
		T_word_ptr					=>	T_word_ptr,
		T_data_to_mem				=>	T_data_to_mem,
		T_data_from_mem			=>	T_data_from_mem,
		T_data_from_mem_latch	=> T_data_from_mem_latch
	);
	
	spi_masterInst : spi_master
	generic map ( slaves => 1, d_width => 10 )
	port map (
		clock		=> clk_50M,
		reset_n	=> spi_rst_n,
		enable	=>	spi_en,
		cpol		=> spi_clk_pol,
		cpha		=>	spi_clk_pha,
		cont		=> spi_cont,
		clk_div	=> spi_clk_div,
		addr		=> spi_slv_addr,
		tx_data	=> spi_tx_data,
		miso		=> SPI_SDI,
		sclk		=> SPI_CLK,
		ss_n		=> SPI_SEL,
		mosi		=> SPI_SDO,
		busy		=> spi_busy,
		rx_data	=> spi_rx_data
	);
	
	ClockCounter: process(clk_50M)
	begin
		if(rising_edge(clk_50M)) then
			clk_cnt<=clk_cnt+1;
		end if;
	end process; -- ClockCounter
	
	DataFromTosNet: process(clk_50M)
	begin
		if(rising_edge(clk_50M) and T_data_from_mem_latch='1') then
			case(T_reg_ptr & T_word_ptr) is
				when "00000" => reg0_0 <= T_data_from_mem(31 downto 0);
				when others =>
			end case;
		end if;
	end process; -- DataFromTosNet
	
	DataToTosNet: process(T_reg_ptr, T_word_ptr)
	begin
		T_data_to_mem <= (others => '0'); -- default data
		case(T_reg_ptr & T_word_ptr) is
			when "00000" => T_data_to_mem <= x"DEADBEEF"; -- Because we love dead beef
			when "00100" => T_data_to_mem <= x"DEADBEEF";
			when "01000" => T_data_to_mem <= x"DEADBEEF";
			when "01100" => T_data_to_mem <= x"DEADBEEF";
			when others =>
		end case;
	end process; -- DataToTosNet
end Behavioral;


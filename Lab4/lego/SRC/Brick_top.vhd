library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Here we define the I/O connections
-- Since this is the top level, the connections all go to the outside world
entity Brick_top is
Port (
      CLK_50M_I	: in	std_logic;	-- 50 MHz from onboard oscillator
		
		UART_RX		: in	std_logic;	-- Serial stream from PC
	   UART_TX		: out	std_logic;	-- Serial stream to PC
		
		CS_O			: out std_logic;
		PULSE_I		: in	std_logic;
		ADCCLK_O		: out	std_logic;
		
		ledRed		: out std_logic := '0';
		ledGreen		: out std_logic := '0';
		ledBlue		: out std_logic := '0';
		
		PWM_OUT		: out std_logic := '0'
	  );
end Brick_top;

architecture Behavioral of Brick_top is
-- Here we define the components we want to include in our design (there is only one)
	COMPONENT PseudoTosNet_ctrl is
	Port (
		T_clk_50M					: in	std_logic;
		T_serial_out				: out std_logic;
		T_serial_in					: in  std_logic;
		T_reg_ptr					: out std_logic_vector(2 downto 0);
		T_word_ptr					: out std_logic_vector(1 downto 0);
		T_data_to_mem				: in  std_logic_vector(31 downto 0);
		T_data_from_mem			: out std_logic_vector(31 downto 0);
		T_data_from_mem_latch	: out std_logic
		);
	END COMPONENT;

-- Here we define the signal used by the top level design
	signal clk_50M						: std_logic;

-- Signals below is used to connect to the Pseudo TosNet Controller component  
	signal T_reg_ptr					: std_logic_vector(2 downto 0);
	signal T_word_ptr					: std_logic_vector(1 downto 0);
	signal T_data_to_mem				: std_logic_vector(31 downto 0);
	signal T_data_from_mem			: std_logic_vector(31 downto 0);
	signal T_data_from_mem_latch	: std_logic;
 
-- ADC
	signal ADC_count					: std_logic_vector(5 downto 0):=(others => '0');
	signal ADC_Clk						: std_logic:='0';
	signal CS_count					: std_logic_vector(5 downto 0):=(others => '0');
	signal CS							: std_logic:='0';
	signal feed							: std_logic:='0';
	signal data							: std_logic_vector(9 downto 0):=(others => '0');
	signal data_tmp					: std_logic_vector(10 downto 0):=(others => '0');
	
-- LED driving
	signal countLed					: std_logic_vector(1 downto 0):=(others => '0');
	signal color						: std_logic_vector(2 downto 0):=(others => '0');
	signal switchLed					: std_logic:='0';
	signal counterADC					: std_logic_vector(2 downto 0):=(others => '0');

-- Servo driving
	signal pwm_out_sig				: std_logic:='0';
	signal reset_servo_counter		: std_logic:='0';
	signal reset_reset_servo_counter		: std_logic:='0';
	signal servo_state_counter		: integer := 0;
	signal clock_divider				: std_logic_vector(7 downto 0) := (others => '0');
	signal count_pulse				: std_logic_vector(12 downto 0) := (others => '0');
	signal servo						: std_logic_vector(7 downto 0) := (others => '0');

begin
-- Here we instantiate the Pseudo TosNet Controller component, and connect it's ports to signals	
	PseudoTosNet_ctrlInst : PseudoTosNet_ctrl
	Port map (
	   T_clk_50M					=> clk_50M,
		T_serial_out				=> UART_TX,
		T_serial_in					=> UART_RX,
		T_reg_ptr					=> T_reg_ptr,
		T_word_ptr					=> T_word_ptr,
		T_data_to_mem				=> T_data_to_mem,
		T_data_from_mem			=> T_data_from_mem,
		T_data_from_mem_latch	=> T_data_from_mem_latch
		);

   clk_50M <= CLK_50M_I;
	PWM_OUT <= pwm_out_sig;
	
-- Mapping the signals for the ADC conversion
	feed		<= PULSE_I;
	CS_O		<= CS;
	ADCCLK_O	<= ADC_Clk;

---------------------------------------------------------
-- Clocked process, to take data off the controller bus/PC	
----------------------------------------------------------
	DatFromTosNet:
	process(clk_50M)
	begin
		if (rising_edge(clk_50M) and T_data_from_mem_latch='1') then
			case (T_reg_ptr & T_word_ptr) is	-- The addresses are concatenated for compact code
				when others =>	-- Not reading anything
			end case;
		end if;
	end process;

----------------------------------------------------------
-- Unclocked process, to place data on the controller bus/PC
----------------------------------------------------------
   DatToTosNet:
	process(T_reg_ptr,T_word_ptr)
	begin
		T_data_to_mem<= (others => '0');	-- default data
		case (T_reg_ptr & T_word_ptr) is	-- The addresses are concatenated for compact code
			when "00000" => T_data_to_mem <= (others => '0');						-- first word is empty
			when "00001" => T_data_to_mem <= (31 downto 3 => '0') & color;		-- second word is for color
			when "00010" => T_data_to_mem <= (31 downto 10 => '0') & data;		-- thirth word is for data
			when others =>
		end case;		
	end process;
  
------------------------------------------------------
-- Lego brick seperator (Lab 4)
------------------------------------------------------
	ADC_Timer:process(clk_50M)
	begin
		if rising_edge(clk_50M) then
			ADC_count<=ADC_count+1;
			if (ADC_count>23) then
				ADC_Clk<=ADC_Clk xor '1';
				ADC_count<=(others => '0');
			end if;
		end if;
	end process;

	CS_setup:process(ADC_Clk)
	begin
		if falling_edge(ADC_Clk) then
			CS_count<=CS_count+1;
			if (CS_count<11) then
				CS<='0';
			else
				CS<='1';
			end if;
			if (CS_count>37) then
				CS_count <= (others => '0');
			end if;
		end if;
	end process;
	
	ADC:process(CS,ADC_Clk)
	begin
		if rising_edge(ADC_Clk) then
			if(CS='0') then
				data_tmp(0)<=feed;
				data_tmp(10 downto 1)<= data_tmp(9 downto 0);
			else
				data<=data_tmp(9 downto 0);
			end if;
			
		end if;
	end process;
	
	LEDREAD:process(CS)		--reading the data to compare which color it should be
	begin
		if falling_edge(CS) then
			if countLed = 0 and data > 700 then
				color(0) <= '1';	--red
				color(1) <= '0';
				color(2) <= '0';
				if(reset_reset_servo_counter = '1') then
					reset_servo_counter <= '0';
				else
					reset_servo_counter <= '1';
				end if;
			elsif countLed = 1 and data > 500 then
				color(0) <= '0';
				color(1) <= '1';	--green
				color(2) <= '0';
				if(reset_reset_servo_counter ='1') then
					reset_servo_counter <= '0';
				else
					reset_servo_counter <= '1';
				end if;
			elsif countLed = 2 and data > 700 then
				color(0) <= '0';
				color(1) <= '0';
				color(2) <= '1';	--blue
				if(reset_reset_servo_counter = '1') then
					reset_servo_counter <= '0';
				else
					reset_servo_counter <= '1';
				end if;
			end if;
		end if;
	end process;
				
	LEDSHIFT:process(CS)		--switching between the sets of LED
	begin
		if rising_edge(CS) then
			if countLed = 0 then
				ledRed	<= '1';
				ledGreen	<= '0';
				ledBlue	<= '0';
			elsif countLed = 1 then
				ledRed	<= '0';
				ledGreen	<= '1';
				ledBlue	<= '0';
			elsif countLed = 2 then
				ledRed	<= '0';
				ledGreen	<= '0';
				ledBlue	<= '1';
			else
				ledRed	<= '0';
				ledGreen	<= '0';
				ledBlue	<= '0';
			end if;
			
			countLed <= countLed+1;
			if countLed >= 2 then
				countLed <= (others => '0');
			end if;
			
		end if;
	end process;
	
	ServoStateManager:process(clk_50M,reset_servo_counter)	--moving the arm
	begin
		if(rising_edge(clk_50M)) then
			if (color = "100") then 
				servo <= x"3F";
			else
				servo <= x"9F";
			end if;
		end if;
	end process;

	ServoDriver:process(clk_50M)
	begin
		if(rising_edge(clk_50M)) then
			clock_divider <= clock_divider+1;
			if(clock_divider > 195) then
				if(count_pulse < 255 + conv_integer(servo)) then
					pwm_out_sig <= '1';
				else
					pwm_out_sig <= '0';
				end if;
				count_pulse <= count_pulse+1;
				clock_divider <= "00000000";
				if count_pulse >5099 then
					count_pulse <= (others => '0');
				end if;
			end if;
		end if;
	end process;
	
end Behavioral;